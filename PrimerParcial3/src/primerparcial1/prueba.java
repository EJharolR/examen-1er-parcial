package primerparcial1;
/**
 *
 * @author Jharol Rodriguez
 */
public class prueba {
    public static double evaluar(String Infija){
        String Posfija = convertir(Infija);
        return evaluarposfija(Posfija);
    }
    private static String convertir(String Infija) {
        String Posfija = "" ;
        pila Pila =new pila(100);
        for(int i=0;i< Infija.length();i++){
            char signo =Infija.charAt(i);
            if(esoperador(signo)) {
                if(Pila.estaVacia()){
                    Pila.apilar(signo);
                }
                else{
                    int pe = prioridadExpresion(signo);
                    int pp = prioridadPila((char)Pila.elementoMax());
                    if (pe>pp){
                        Pila.apilar(signo);  
                    }
                    else{
                        Posfija += Pila.desapilar();
                        Pila.apilar(signo);
                    }    
                }
            }
            else {
               Posfija += signo;
           }
        }
        while(!Pila.estaVacia()){
            Posfija += Pila.desapilar();
        }
        return Posfija;
    }
    private static int prioridadExpresion(char operador){
        if(operador == '*') return 4;
        if(operador == '*' || operador == '/' ) return 2;
        if(operador == '+' || operador == '-' ) return 1;
        if(operador == '(' ) return 5;
        return 0;
    } 
    private static int prioridadPila(char operador)
    {
        if(operador == '*') return 3;
        if(operador == '*' || operador == '/' ) return 2;
        if(operador == '+' || operador == '-' ) return 1;
        if(operador == '(' ) return 0;
        return 0;
    }
    private static double evaluarposfija(String Posfija) {
        pila Pila = new pila(100);
        for(int i = 0;i < Posfija.length();i++){
            char signo =Posfija.charAt(i);
            if(!esoperador(signo)){
                double x = new Double(signo + " ");
                Pila.apilar(x);
            }
            else{
                double num2 =(double)Pila.desapilar();
                double num1 =(double)Pila.desapilar();
                double num3 = operacion(signo,num1,num2);
                Pila.apilar(num3);
            }
        }
        return(double)Pila.elementoMax();
    }
    private static boolean esoperador(char signo) {
        if (signo == '*' || signo == '/' || signo == '+' || signo == '-' || signo == '('|| signo == ')'){
            return true;       
        }   
        return false;
    }
    private static double operacion(char signo, double num1, double num2) {
        if(signo =='*')return num1*num2;
            if(signo =='/')return num1/num2;
                if(signo =='+')return num1+num2;
                    if(signo =='-')return num1-num2;
                        if(signo =='*')return Math.pow(num1,num2);
                            return 0;
    }
}
