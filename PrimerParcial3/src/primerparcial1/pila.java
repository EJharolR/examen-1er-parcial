package primerparcial1;
/**
 *
 * @author Jharol Rodriguez
 */
public class pila {
    private int N;
    private int max;
    private Object Pila[];
    public pila(int N) {
        this.N = N;
        max = 0;
        Pila = new Object [N];
    }
    public boolean estaVacia(){
        return max ==0;
    }
    public boolean estaLlena(){
        return max ==N;
    }
    public boolean apilar(Object x){
        if(estaLlena()) {
            return false;
    }
        Pila[max] = x;
        max ++;
        return true;
    }
    public Object desapilar(){
        if(estaVacia()){
            return null;
        }
        max--;
        return Pila[max];
    }
    public Object elementoMax(){
       return Pila[max -1];
    }
}

