package primerparcial2;
/**
 *
 * @author Jharol Rodriguez
 */
class stackDinamicOne {
    char arr[];
    int tope;
    stackDinamicOne(){
        this.arr = new char[5];
        this.tope = -1;
    }
    public boolean isEmpty(){
        return (tope == -1);
    }
    public void push(char element){
        tope++;
        if(tope < arr.length){
            arr[tope] = element;
        }
        else{
            char temp[] = new char[arr.length+5];
            for(int i = 0; i<arr.length; i++){
                temp[i] = arr[i];
            }
            arr = temp;
            arr[tope] = element;
        }
    }
    public char peek(){
        return arr[tope];
    }
    public char pop(){
        if(!isEmpty()){
            int temptop = tope;
            tope--;
            char returntop = arr[temptop];
            return returntop;
        }else{
            return '-';
        }
    }    
}
